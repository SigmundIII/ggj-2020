﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Highlight : MonoBehaviour {
    private SpriteRenderer renderer;
    public Sprite original;
    public Sprite highlight;

    public float flickerDelta = 0.8f;
    private float time = 0;

    private bool flickering = false;

    private void Awake() {
        renderer = GetComponent<SpriteRenderer>();

        if(!original)
            Debug.Log(name + " - Button [Awake]: Missing Original Sprite reference.");
        if(!highlight)
            Debug.Log(name + " - Button [Awake]: Missing Hightlight Sprite reference.");
    }

    private void Update() {
        if(flickering) {
            time += Time.deltaTime;

            if(time > flickerDelta) {
                time = 0;
                if(renderer.sprite == original)
                    renderer.sprite = highlight;
                else
                    renderer.sprite = original;
            }
        }
    }

    public bool IsFlickering() {
        return flickering;
    }

    public void StartFlickering() {
        flickering = true;
    }

    public void StartFlickering(float delta) {
        flickering = true;
        flickerDelta = delta;
    }

    public void StopFlickering() {
        flickering = false;
    }


    private void OnMouseEnter() {
        renderer.sprite = highlight;
    }

    private void OnMouseOver() {
        renderer.sprite = highlight;
    }

    private void OnMouseExit() {
        renderer.sprite = original;
    }
}
