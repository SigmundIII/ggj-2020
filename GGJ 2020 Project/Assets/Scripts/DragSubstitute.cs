﻿using UnityEngine;

public class DragSubstitute : Drag {
    public Transform destination;

    public float magnetRange = 0.5f;

    private bool isInMagnetRange = false;

    private void Awake() {
        if(!destination)
            Debug.LogError(name + " - DragSubstitute [Awake]: Missing Destination reference.");
    }

    private void OnDrawGizmos() {
        Gizmos.DrawWireSphere(destination.position, magnetRange);
    }

    void Update()
    {
        if(IsDragged()) {
            if(Vector3.Distance(transform.position, destination.position) < magnetRange) {
                isInMagnetRange = true;
            } else
                isInMagnetRange = false;
        } else if(isInMagnetRange) {
            transform.position = destination.position;
            GetComponent<GoalObject>().Complete();
        } else
            transform.position = startPosition;
    }
}
