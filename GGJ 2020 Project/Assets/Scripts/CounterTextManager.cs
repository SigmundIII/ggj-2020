﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CounterTextManager : MonoBehaviour
{

    public Text counterText;
    private int counter;

    void Start()
    {
        counterText = GetComponent<Text>();
        counter = FindObjectOfType<GameManager>().CounterText();
    }

    void Update()
    {
        counter = FindObjectOfType<GameManager>().CounterText();
        counterText.text = counter.ToString();
    }
}
