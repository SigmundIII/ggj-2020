﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoverMovedController : MonoBehaviour //La classe serve unicamente a capire se la cover si trova all'interno del collider della starting position o meno, quindi il bool viene passato alla classe del drag and drop che fa una verifica sul bool e sposta il cubo
{
    public static bool outside = false;
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Cover"))
        {
            outside = false;
        }
    }

    private void OnTriggerExit(Collider collision)
    {

        if (collision.gameObject.CompareTag("Cover"))
        {
            Debug.Log("TriggerExit");
            outside = true;
        }

    }

    
}
