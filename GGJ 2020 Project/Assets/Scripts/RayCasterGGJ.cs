﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCasterGGJ : MonoBehaviour
{
    public static int counter = 0;
    public GameObject cover;
    void Start()
    {

    }

    void Update()
    {
        Raycast();
    }


    void Raycast()
    {
        if (Input.GetMouseButtonDown(0))
        {

            Vector2 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(worldPoint, Vector2.zero);
            if (hit.collider != null)
            {
                if (hit.collider.CompareTag("Screw"))
                {
                    if (cover.transform.position == DragAndDropController.startingPosition)
                    {

                        if (hit.collider.GetComponent<Renderer>().enabled == false)
                        {
                            if (DragAndDropController.isCovered == true)
                            {
                                hit.collider.GetComponent<Renderer>().enabled = true;
                                counter -= 1;
                                //Debug.Log(counter);
                            }
                        }
                        else
                        {
                            hit.collider.GetComponent<Renderer>().enabled = false;
                            counter += 1;
                            //Debug.Log(counter);
                        }
                    }
                }
            }
        }
    }
}
