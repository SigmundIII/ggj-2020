﻿using UnityEngine;

public class ViewHandler : MonoBehaviour {
    public GameObject radioViewPosition;
    public GameObject tableViewPosition;
    public AudioSource bgm;
    public AudioSource am;
    public AudioSource fm;

    public float movingSpeed = 5f;

    public bool isInRadioView = true;
    private bool changeView = false;

    private void Update() {
        if(!changeView) {
            RayCast();
        } else {
            if (!isInRadioView) {
                transform.position = Vector3.MoveTowards(transform.position, tableViewPosition.transform.position, movingSpeed * Time.deltaTime);
                if(transform.position == tableViewPosition.transform.position) {
                    changeView = false;
                    fm.volume = 0;
                    am.volume = 0;
                    bgm.volume = 1f;
                }
                    
            } else {
                transform.position = Vector3.MoveTowards(transform.position, radioViewPosition.transform.position, movingSpeed * Time.deltaTime);
                if (transform.position == radioViewPosition.transform.position) {
                    changeView = false;
                    fm.volume = 1f;
                    am.volume = 1f;
                    bgm.volume = 0;
                }
                    
            }
        }
    }

    private void RayCast() {
        if(Input.GetMouseButtonDown(0)) {
            Vector2 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(worldPoint, Vector2.zero);
            if(hit.collider != null) {
                if(hit.collider.gameObject.CompareTag("ToggleViewButton")) {
                    if(isInRadioView) {
                        isInRadioView = false;
                        changeView = true;
                    } else {
                        isInRadioView = true;
                        changeView = true;
                    }
                }
            }
        }
    }

    public bool IsInRadioView() {
        return isInRadioView;
    }

    public bool IsChangingView()
    {
        return changeView;
    }
}
