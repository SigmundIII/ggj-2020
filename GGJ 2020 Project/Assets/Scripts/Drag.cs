﻿using UnityEngine;

public class Drag : MonoBehaviour {
    private Vector3 offset;
    
    protected Vector3 startPosition;
    private bool isDragged = false;
    public SpriteRenderer renderer;
    public Sprite original;
    public Sprite highlight;

    protected SoundManager sm;


    private void Start() {
        startPosition = transform.position;
        sm = FindObjectOfType<SoundManager>();
        if (!sm) {
            Debug.LogError("Manca il sound manager in game manager");
        }
    }

    private void OnMouseEnter() {
        if(highlight)
            renderer.sprite = highlight;
    }

    private void OnMouseExit()
    {
        isDragged = false;
        renderer.sprite = original;
    }

    private void OnMouseDown() {
        offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, startPosition.z));
        sm.PlaySound("PickSound");
    }

    private void OnMouseDrag() {
        if(highlight)
            renderer.sprite = highlight;
        isDragged = true;
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, startPosition.y);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        curPosition = new Vector3(curPosition.x, curPosition.y, startPosition.z);
        transform.position = curPosition;
    }

    private void OnMouseUp() {
        sm.PlaySound("DropSound");
        isDragged = false;
        renderer.sprite = original;
        FindObjectOfType<GameManager>().UpdateLose();
        
    }

    public bool IsDragged() {
        return isDragged;
    }
}