﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    public RotateTacchetta[] centroTacchette;
    public float rotationSpeed = 1;
    private float angle = 0;
    public float deltaAngle = 90f;
    private Vector3 dragOrigin;
    public float currentAngle;
    public float speed=1;

    public bool fm;

    private void Awake() {
        centroTacchette = FindObjectsOfType<RotateTacchetta>();
    }

    private void OnMouseDown() {
        dragOrigin = Input.mousePosition;
        angle = transform.rotation.eulerAngles.z;
        Cursor.visible = false;
    }


    void OnMouseDrag() {
        var dragPosition=Input.mousePosition.x - dragOrigin.x;
         currentAngle= transform.rotation.eulerAngles.z;
        if (currentAngle >= 180) {
            currentAngle -= 360;
        }
        angle = 0;
        //angoli invertiti
        var dir = new Vector3(0, 0, 0);
        if (dragPosition < 0) { //drag verso sinistra
            dir= new Vector3(-1, 0, 0);
            if (currentAngle < deltaAngle) {
                angle+=rotationSpeed;
            } else {
                dir = new Vector3(0, 0, 0);
                Debug.Log("Cant drag to left anymore ");
            }

        } else if(dragPosition > 0) { //drag verso destra
            dir = new Vector3(+1, 0, 0);

            if (currentAngle> -deltaAngle) {
                angle-=rotationSpeed;
            } else {
                dir = new Vector3(0, 0, 0);
                Debug.Log("Cant drag to right anymore");
            }
        }

        dragOrigin = Input.mousePosition;
        dir=dir.normalized;
        transform.position += dir * speed *speed;
        transform.Rotate(0, 0, angle);
        
        for(int i=0; i< centroTacchette.Length; i++) {
            if (fm==centroTacchette[i].fm) {
                centroTacchette[i].transform.Rotate(0, 0, angle);
            }
        }
        
    }

    private void OnMouseUp() {
        Cursor.visible = true;
    }
}
