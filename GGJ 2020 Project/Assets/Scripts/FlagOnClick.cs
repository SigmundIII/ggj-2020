﻿using UnityEngine;

public class FlagOnClick : MonoBehaviour {
    private bool flagged = false;

    public bool IsFlagged() {
        return flagged;
    }

    private void OnMouseDown() {
        flagged = true;
    }
}
