﻿using UnityEngine;

public class PlayMusic : MonoBehaviour
{
    private AudioSource song;
    private AudioDistortionFilter adf;
    private CheckFrequency cf;
    public float state;
    public bool fm;
    public ViewHandler vh;

    private void Awake() {
        cf = GetComponent<CheckFrequency>();
        song= GetComponent<AudioSource>();
        if (!song) {
            Debug.LogError("Error"+name);
        }
        adf = GetComponent<AudioDistortionFilter>();
        state = cf.GetState();
        song.Play();
        song.volume = 1;
    }

    // Update is called once per frame
    void Update() {
        state = cf.GetState();
        if(state > 0.99f)
            state = 0.99f;
        adf.distortionLevel = state;
        if (vh.isInRadioView) {
            song.volume = 1 - (state / 1.5f);
        }
    }

    private void OnEnable() {
        song.Play();
        Debug.Log("Canzone "  + song.name);
    }

    private void OnDisable() {
        song.Pause();
    }
}
