﻿using UnityEngine;

public class PanelDragController : MonoBehaviour
{

    private Vector3 screenPoint;
    private Vector3 offset;
    private Vector3 startingPosition;
    public float minDistanceToMagnet = 3f;
    public bool drawGizmos = false;
    public float returnSpeed = 1f;
    private bool returned = false;

    void Start()
    {
        startingPosition = transform.position;
    }

    private void OnDrawGizmos() {
        if(!drawGizmos)
            return;

        Gizmos.DrawWireSphere(transform.position, minDistanceToMagnet);
    }

    void OnMouseDown()
    {
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
    }

    void OnMouseDrag()
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        transform.position = curPosition;
    }

    private void OnMouseUp() {
        if (Vector3.Distance(transform.position, startingPosition) <= minDistanceToMagnet)
        {
            transform.position = startingPosition;
            FindObjectOfType<GameManager>().UpdateLose();
        }
        else
            FindObjectOfType<GameManager>().UpdateLose();
    }

    public void ReturnToOrigin() {
        if(transform.position == startingPosition)
            returned = true;
        else
        {
            Vector3.MoveTowards(transform.position, startingPosition, returnSpeed * Time.deltaTime);
        }
    }

    public bool IsReturned() {
        return returned;
    }
}