﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    public GameObject textPanel;
    public Text text;

    public SwitchSprite radioSpriteSwitcher;

    public GoalObject[] objectives;
    public string[] hints;
    public PanelDragController[] covers;

    private ViewHandler viewHandler;
    private int currentIndex = 0;

    private bool completedAllObjectives = false;

    public CheckFrequency am;
    public CheckFrequency fm;

    public int movesCounter = 0;
    public int maxMoves;

    public GameObject gameOverScreen;

    public string nextLevel;

    public int frequenze;
    private int remainingMoves;

    private bool isInRadioView;
    private bool isChangingView;

    public bool isTutorial = false;

    public GameObject counterText;

    private float timer;

    private SoundManager sm;

    private void Awake() {
        if(!textPanel)
            Debug.LogError(name + " - GameManager [Awake]: Missing Text Panel reference.");
        if(!text)
            Debug.LogError(name + " - GameManager [Awake]: Missing Text reference.");

        if(!radioSpriteSwitcher)
            Debug.LogError(name + " - GameManager [Awake]: Missing Radio Sprite Switcher reference.");

        if(!am)
            Debug.LogError(name + " - GameManager [Awake]: Missing AM reference.");
        if(!fm)
            Debug.LogError(name + " - GameManager [Awake]: Missing FM reference.");

        viewHandler = FindObjectOfType<ViewHandler>();
        if (!viewHandler) {
            Debug.LogError("Main Camera mancante in " + name);
        }

        sm=GetComponent<SoundManager>();
        if (!sm) {
            Debug.LogError("Manca il sound manager in game manager");
        }
    }

    private void Update() {
        isInRadioView = viewHandler.IsInRadioView();
        isChangingView = viewHandler.IsChangingView();

        if(!isTutorial)
            CounterTextController();

        if(!isInRadioView) {
            HideText();
        } else {
            if(frequenze == 2) {
                radioSpriteSwitcher.SetAlternative();
                if(am.syncronized && fm.syncronized) {
                    ShowText();
                } else {
                    HideText();
                }
            } else if(frequenze == 1) {
                radioSpriteSwitcher.SetOriginal();
                if(am.syncronized || fm.syncronized) {
                    ShowText();
                } else {
                    HideText();
                }
            }
        }

        UpdateVictory();
    }

    private void UpdateVictory() {
        int i = 0;
        foreach(GoalObject objective in objectives){
            if(objective.IsCompleted()) {
                i++;
                if(i < objectives.Length)
                    currentIndex = i;
            } else
                break;
        }

        if (i > objectives.Length - 1) {
            completedAllObjectives = true;
            sm.PlaySound("WinSound");
            //Debug.Log("HAI VINTOOO!!!!");
            bool allReturned = true;
            foreach(PanelDragController cover in covers) {
                if(!cover.IsReturned()) {
                    cover.ReturnToOrigin();
                    allReturned = false;
                }
            }

            if(allReturned) {
                gameOverScreen.SetActive(true);
                bool canProcede = gameOverScreen.GetComponent<FlagOnClick>().IsFlagged();
                if(canProcede)
                    SceneManager.LoadScene(nextLevel);
            }       
        }
    }

    public void UpdateLose() {
        movesCounter += 1;
        //Debug.Log(movesCounter);

        if(movesCounter >= maxMoves) {
            sm.PlaySound("LooseSound");
            //Debug.Log("GAME OVER");
            Time.timeScale = 0;
            gameOverScreen.SetActive(true);
        }
    }

    private void CounterTextController() {
        if(!counterText.activeInHierarchy) {
            if(!isChangingView && !isInRadioView)
                counterText.SetActive(true);
        } else {
            if(isChangingView || isInRadioView)
                counterText.SetActive(false);
        }
    }

    public void ShowText() {
        text.text = hints[currentIndex];
        textPanel.SetActive(true);
    }

    public void HideText() {
        textPanel.SetActive(false);
    }

    public bool HasCompletedAllObjectives() {
        return completedAllObjectives;
    }

    public bool IsInRadioView() {
        return viewHandler.IsInRadioView();
    }

    public bool IsChangingView() {
        return isChangingView;
    }

    public int CounterText()
    {
        remainingMoves = maxMoves - movesCounter;
        return remainingMoves;
    }

    public bool CheckObjectives()
    {
        return completedAllObjectives;
    }

}
