﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeFrequency : MonoBehaviour
{
    public float range;
    public float deltaRange;
    public float deltaTime=7;

    private float time = 0;
    private float maxRange;
    private float minRange;
    public Rotate r;
    private GameManager gm;

    private bool found = false; //il player ha trovato la frequenza?
    private bool reset = false; //il player ha cambiato resettato la frequenza?

    public bool fm=true;

    public CheckFrequency cf;

    private void Awake() {
        if (!r) {
            Debug.LogError("Rotate script not found in " + name);
        }
        gm = FindObjectOfType<GameManager>();
        if (!gm) {
            Debug.LogError("Game manager not found in " + name);
        }
        cf = GetComponent<CheckFrequency>();
        GenerateRange();
    }

    // Update is called once per frame
    void Update(){
        if (!gm.IsInRadioView() && !reset) {
            //reset quando entra nella view della radio
            GenerateRange();
            reset = true;
            found = false;
            gm.HideText();
            //Debug.Log("Testo tolto!");
            cf.syncronized = false;
            
        } else if(!gm.IsInRadioView()){
            reset = false;
        }

        if (!found) { //reset se non trovata entro deltaTime
            if (time < deltaTime) {
                time += Time.deltaTime;
            } else {
                time = 0;
                GenerateRange();
                //Debug.Log(deltaTime + "second passed");
            }
        }
    }

    public float GetMaxRange() {
        return maxRange;
    }

    public float GetMinRange() {
        return minRange;
    }

    public float GetDeltaRange() {
        return deltaRange;
    }

    public void Found(bool f) {
        found = f;
    }

    public void GenerateRange() {
        minRange = Random.Range(-r.deltaAngle, r.deltaAngle - deltaRange);
        maxRange = minRange + range;
        //Debug.Log("Minimum range: " + minRange);
    }
}
