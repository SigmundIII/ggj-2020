﻿using UnityEngine;

public class TextPanelScript : MonoBehaviour {
    public float disappearDelta = 5f;
    private float time = 0;

    public bool disappearOnClick = true;

    #region DEBUG
    public bool debugEnables = false;

    private void OnEnable() {
        if(debugEnables)
            Debug.Log("Enabled " + name);
    }

    private void OnDisable() {
        if(debugEnables)
            Debug.Log("Disabled " + name);
    }
    #endregion

    private void Update() {
        if(!disappearOnClick) {
            time += Time.deltaTime;

            if(time > disappearDelta) {
                time = 0;
                gameObject.SetActive(false);
            }
        } else {
            if(Input.GetMouseButtonDown(0)) {
                gameObject.SetActive(false);
            }
        }
    }
}
