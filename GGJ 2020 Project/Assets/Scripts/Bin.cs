﻿using UnityEngine;

public class Bin : MonoBehaviour {

    private void OnTriggerStay(Collider other) {
        if(other.gameObject.CompareTag("DraggedObject")) {
            if(!other.gameObject.GetComponent<Drag>().IsDragged())
                Destroy(other.gameObject);
        }
    }
}
