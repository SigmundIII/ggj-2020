﻿using UnityEngine;

public class DragDisposable : Drag {
    public Transform destination;

    public float magnetRange = 0.5f;
    public Vector3 magnetForm = new Vector3(1, 2, 1);
    public GameObject endingPosition;

    private bool isInMagnetRange = false;

    private void Awake() {
        if(!destination)
            Debug.LogError(name + " - DragDisposable [Awake]: Missing Destination reference.");
    }

    private void OnDrawGizmos() {
        Gizmos.DrawWireCube(destination.position, magnetForm * magnetRange);
    }

    private void Update() {
        if(IsDragged()) {
            if(Vector3.Distance(transform.position, destination.position) < magnetRange) {
                isInMagnetRange = true;
            } else {
                isInMagnetRange = false;
            }
        } else {
            if(isInMagnetRange) {
                GetComponent<GoalObject>().Complete();
                transform.position = endingPosition.transform.position;
                sm.PlaySound("GarbageSound");
            } else
                transform.position = startPosition;
        }
    }
}
