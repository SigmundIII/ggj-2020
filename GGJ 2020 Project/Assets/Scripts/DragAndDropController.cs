﻿using UnityEngine;

public class DragAndDropController : MonoBehaviour
{

    private Vector3 screenPoint;
    private Vector3 offset;
    public static Vector3 startingPosition;
    public static Vector3 coverPosition;
    public GameObject coverEndingPosition;
    public GameObject coverStartingPosition;
    public static bool isCovered = true;
    public int screwNumber;

    void Start()
    {
        startingPosition = gameObject.transform.position;
    }

    void OnMouseDown()
    {
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
        //sound grab
    }

    void OnMouseDrag()
    {
        if(RayCasterGGJ.counter == screwNumber) // Drag della cover in relazione alla rimozione di tutte le viti
        {
            Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
            Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
            transform.position = curPosition;
        }
    }

    void OnMouseUp()
    {
        if (CoverMovedController.outside == true) // Controllo sul bool del trigger per la starting position
        {
            gameObject.transform.position = coverEndingPosition.transform.position;
            isCovered = false;
        }
        else
        {
            gameObject.transform.position = coverStartingPosition.transform.position;
            isCovered = true;
        }
        //sound drop
    }

}