﻿using UnityEngine;

public class TutorialButton : MonoBehaviour {
    public Tutorial tutorial;

    private void Awake() {
        if(!tutorial)
            Debug.Log(name + " - TutorialButton [Awake]: Missing Tutorial reference.");
    }

    private void OnMouseDown() {
        tutorial.PressedChangeSceneButton();
    }
}
