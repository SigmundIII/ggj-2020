﻿using UnityEngine;

public class Tutorial : MonoBehaviour {
    private enum TutorialPhase {SPAWN, FIRST_TOUCH, SEE_RADIO, WAIT_FOR_FREQ, FOUND_FREQ, LVL_FINISH};
    private TutorialPhase currentPhase = TutorialPhase.SPAWN;

    public GameObject textPanelFirst;
    public GameObject textPanelWait;
    public GameObject textPanelRadioUse;
    public GameObject textPanelMiddle;
    public GameObject textPanelLast;

    public GameObject radioChangeSceneButton;

    public Highlight changeSceneButton;

    public GameObject glass;
    private Vector3 glassOrigin;

    private float timer;

    private bool checkClick = false;

    private void Awake() {
        if(!textPanelFirst)
            Debug.Log(name + " - Tutorial [Awake]: Missing Text Panel First reference.");
        if(!textPanelWait)
            Debug.Log(name + " - Tutorial [Awake]: Missing Text Panel Wait reference.");
        if(!textPanelRadioUse)
            Debug.Log(name + " - Tutorial [Awake]: Missing Text Panel Radio Use reference.");
        if (!textPanelMiddle)
            Debug.Log(name + " - Tutorial [Awake]: Missing Text Middle Radio Use reference.");
        if (!textPanelLast)
            Debug.Log(name + " - Tutorial [Awake]: Missing Text Last Radio Use reference.");
        if (!radioChangeSceneButton)
            Debug.Log(name + " - Tutorial [Awake]: Missing Radio Change Scene Button reference.");

        if(!changeSceneButton)
            Debug.Log(name + " - Tutorial [Awake]: Missing Change Scene Button reference.");

        if(!glass)
            Debug.Log(name + " - Tutorial [Awake]: Missing Glass reference.");
        glassOrigin = glass.transform.position;
    }

    private void Update() {
        switch(currentPhase) {
        case TutorialPhase.SPAWN:
            if(glass.transform.position != glassOrigin) {
                currentPhase = TutorialPhase.FIRST_TOUCH;
                glass.transform.position = glassOrigin;
                textPanelFirst.SetActive(false);
                textPanelWait.SetActive(true);
            }
            break;
        case TutorialPhase.FIRST_TOUCH:
            glass.transform.position = glassOrigin;
            changeSceneButton.StartFlickering();
            break;
        case TutorialPhase.SEE_RADIO:
            changeSceneButton.StopFlickering();
            textPanelWait.SetActive(false);
            textPanelRadioUse.SetActive(true);
            currentPhase = TutorialPhase.WAIT_FOR_FREQ;
            break;
        case TutorialPhase.WAIT_FOR_FREQ:
            if(GetComponent<GameManager>().fm.syncronized)
                currentPhase = TutorialPhase.FOUND_FREQ;
            break;
        case TutorialPhase.FOUND_FREQ:
            radioChangeSceneButton.SetActive(true);
            if(!GetComponent<GameManager>().IsInRadioView() && !GetComponent<GameManager>().IsChangingView()) {
                textPanelMiddle.SetActive(true);
                currentPhase = TutorialPhase.LVL_FINISH;
            }
            break;
        case TutorialPhase.LVL_FINISH:           
            if (GetComponent<GameManager>().CheckObjectives())
            {
                textPanelMiddle.SetActive(false);
                textPanelLast.SetActive(true);
            }   
            break;
        }
    }

    public void PressedChangeSceneButton() {
        currentPhase = TutorialPhase.SEE_RADIO;
    }
}
