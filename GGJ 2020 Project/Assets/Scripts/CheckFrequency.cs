﻿using UnityEngine;


public class CheckFrequency : MonoBehaviour{
    public float remainingTime = 3f;
    private float time = 0;

    private GameManager gameManager;
    private ChangeFrequency cf;
    private float currentAngle;

    private float state; //0 trovata, 0-1 trovata poco, 1 non trovata
    public bool syncronized = false;
    
    private void Awake() {
        Init();
    }

    private void Init() {
        gameManager = FindObjectOfType<GameManager>();
        cf = GetComponent<ChangeFrequency>();
        currentAngle = transform.rotation.eulerAngles.z;
        state = 0;
        time = 0;
    }

    private void Update() {
        //rende gli angoli da 180 a -180 invece che da 0 a 360;
        currentAngle = transform.rotation.eulerAngles.z;
        if (currentAngle >= 180) {
            currentAngle -= 360;
        }

        if (currentAngle<=cf.GetMaxRange() && currentAngle >= cf.GetMinRange()) {
            state = 0;
            //Debug.Log("Hai trovato la frequenza");
            cf.Found(true);
            if (time < remainingTime) {
                time += Time.deltaTime;
            } else {
                time = 0;
                //Debug.Log(remainingTime + "second passed");
                syncronized = true;
            }
        } else if(currentAngle> cf.GetMinRange() - cf.GetDeltaRange() && currentAngle< cf.GetMaxRange() + cf.GetDeltaRange()){
            //quasi ok
            state = GetDeltaState();
            //Debug.Log("Ci sei quasi");
            cf.Found(false);
        } else {
            //not ok
            //Debug.Log("Acqua");
            cf.Found(false);
            state = 0.99f;
        }
    }

    private float GetDeltaState() {
        float distortionState=0;
        if(currentAngle> cf.GetMaxRange() && currentAngle <= cf.GetMaxRange() + cf.GetDeltaRange()) {
            distortionState = currentAngle - cf.GetMaxRange();
        }else if (currentAngle< cf.GetMinRange() && currentAngle >= cf.GetMinRange() - cf.GetDeltaRange()) { 
            distortionState = currentAngle - cf.GetMaxRange();
        }
        if (distortionState < 0){
            distortionState *= -1;
        }
        //return a float from 0 to 0.99 based on the deltaCurrentAngle
        distortionState = (distortionState * 0.98f) / cf.GetDeltaRange();
        return distortionState;
    }
    public float GetState() {
        return state;
    }
}
