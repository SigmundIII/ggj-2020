﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamPosition : MonoBehaviour
{
    
    float count = 0f;
    public GameObject ground1;
    public GameObject ground2;
    private void Awake()
    {
        ground1.SetActive(true);
        ground2.SetActive(false);
    }
    //Camnbia la posizione della camera
    public void CameraPosition()
    {
        count++;
        if(count == 1)
        {
            ground1.SetActive(false);
            ground2.SetActive(true);
        }
        if (count == 2)
        {
            ground1.SetActive(true);
            ground2.SetActive(false);
            count = 0;
        }


        //transform.position = new Vector3(0, 0, 0);
    }

    public void CameraPositionInit()
    {
        transform.position = new Vector3(0, 0, 0);
    }
    
}
