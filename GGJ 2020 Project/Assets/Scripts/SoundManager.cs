﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioSource[] audioSources;

    /* list of the sound effects:
     * PickSound -> An item is grabbed;
     * DropSound -> An item is Dropped;
     * GarbageSound-> An item is Garbaged;
     * WinSound -> win game;
     * LooseSound -> Lost game;
     * ObjectiveSound -> An objective is completed
     * */

    public void PlaySound(string name) {
        for(int i=0; i < audioSources.Length; i++) {
            if (audioSources[i].name == name) {
                audioSources[i].Play();
                break;
            }
        }
    }
}
