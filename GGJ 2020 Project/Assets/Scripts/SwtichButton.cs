﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwtichButton : MonoBehaviour{
    private Rotate r;
    private float tempCurrentAngle;
    public PlayMusic am;
    public PlayMusic fm;
    private GameManager gameManager;
    public GameObject fLux;
    public GameObject aLux;

    private void Awake() {
        r = FindObjectOfType<Rotate>();
        gameManager = FindObjectOfType<GameManager>();
    }

    private void OnMouseDown() {
        r.fm = !r.fm;
        if (am.fm == r.fm) {
            //attiva fm
            r.centroTacchette[0].tempRotation = r.currentAngle;
            am.gameObject.SetActive(false);
            aLux.SetActive(false);
            fLux.SetActive(true);
            fm.gameObject.SetActive(true);
            r.transform.rotation = Quaternion.Euler(0, 0, r.centroTacchette[1].tempRotation);
            r.currentAngle = r.centroTacchette[1].tempRotation;
        } else {
            //attiva am
            r.centroTacchette[1].tempRotation = r.currentAngle;
            fm.gameObject.SetActive(false);
            fLux.SetActive(false);
            aLux.SetActive(true);
            am.gameObject.SetActive(true);
            r.transform.rotation = Quaternion.Euler(0, 0, r.centroTacchette[0].tempRotation);
            r.currentAngle = r.centroTacchette[0].tempRotation;
        }

    }
}
