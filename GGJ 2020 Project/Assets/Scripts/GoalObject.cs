﻿using UnityEngine;

public class GoalObject : MonoBehaviour {
    private SoundManager sm;

    private bool completed = false;


    private void Awake() {
        sm = FindObjectOfType<SoundManager>();
        if (!sm) {
            Debug.LogError("Manca il sound manager in game manager");
        }
    }

    public void Complete() {
        completed = true;
        sm.PlaySound("ObjectiveSound");
    }

    public bool IsCompleted() {
        return completed;
    }
}
