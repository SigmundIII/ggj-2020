﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

    public void Play(string sceneName) {
        SceneManager.LoadScene(sceneName);
    }

    public void Quit()
    {
        Debug.Log("EXIT");
        Application.Quit();
    }
}
