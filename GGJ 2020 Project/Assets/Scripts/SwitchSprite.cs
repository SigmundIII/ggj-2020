﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class SwitchSprite : MonoBehaviour {
    private SpriteRenderer renderer;
    public Sprite original;
    public Sprite alternative;

    private void Awake() {
        renderer = GetComponent<SpriteRenderer>();

        if(!original)
            Debug.Log(name + " - Button [Awake]: Missing Original Sprite reference.");
        if(!alternative)
            Debug.Log(name + " - Button [Awake]: Missing Alternative Sprite reference.");
    }

    public void SwitchSprites() {
        if(renderer.sprite == original)
            renderer.sprite = alternative;
        else
            renderer.sprite = original;
    }

    public void SetOriginal() {
        renderer.sprite = original;
    }

    public void SetAlternative() {
        renderer.sprite = alternative;
    }

}
